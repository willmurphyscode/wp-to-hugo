Gem::Specification.new do |spec|
  spec.name        = 'wp_to_hugo'
  spec.version     = '0.0.0'
  spec.date        = '2019-09-16'
  spec.summary     = 'Move from WordPress to Hugo'
  spec.description = 'Convert WordPress Export XML file into markdown suitable for use with Hugo' # rubocop:disable Metrics/LineLength
  spec.authors     = ['Will Murphy']
  spec.email       = 'willmurphyscode@gmail.com'
  spec.files       = ['lib/wp_to_hugo.rb',
                      'lib/wp_to_hugo/post.rb',
                      'lib/wp_to_hugo/gist.rb',
                      'lib/wp_to_hugo/word_press_xml.rb',
                      'lib/wp_to_hugo/post_writer.rb',
                      'lib/wp_to_hugo/custom_converters.rb']
  # spec.homepage = TODO
  spec.license       = 'MIT'
  spec.add_runtime_dependency 'nokogiri', '~> 1.10.4'
  spec.add_runtime_dependency 'reverse_markdown', '~> 1.3.0'
  spec.add_development_dependency 'nokogiri', '~> 1.10.4'
  spec.add_development_dependency 'reverse_markdown', '~> 1.3.0'
  spec.add_development_dependency 'rubocop'
  spec.executables << 'wp_to_hugo'
  spec.require_paths = ['lib']
end
