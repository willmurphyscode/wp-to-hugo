require 'minitest/autorun'
require 'nokogiri'

require './lib/wp_to_hugo'

class PostTest < Minitest::Test
  # rubocop:disable Layout/IndentHeredoc
  EXAMPLE_DOC = <<~XML.freeze
  <?xml version="1.0" encoding="UTF-8"?>
  <rss version="2.0" xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:wp="http://wordpress.org/export/1.2/">
  <item>
  <content:encoded><![CDATA[This body post has 2 paragraphs.

WorpPress doens't put actual p tags in its export, just linebreaks.

Here's some inline code snippets: <code>chmod</code> or <code>chown</code>

Here's a code sample we don't want to mangle:
<div class="sourceCode">
<pre class="sourceCode rust"><code class="sourceCode rust">
<span class="at">#[</span>derive<span class="at">(</span><span class="bu">Clone</span><span class="at">,</span> <span class="bu">Copy</span><span class="at">,</span> <span class="bu">PartialEq</span><span class="at">,</span> <span class="bu">PartialOrd</span><span class="at">,</span> <span class="bu">Eq</span><span class="at">,</span> <span class="bu">Ord</span><span class="at">,</span> <span class="bu">Debug</span><span class="at">,</span> <span class="bu">Hash</span><span class="at">)]</span>

<span class="at">#[</span>must_use <span class="at">=</span> <span class="st">"this `Result` may be an `Err` variant, which should be handled"</span><span class="at">]</span>
<span class="at">#[</span>stable<span class="at">(</span>feature <span class="at">=</span> <span class="st">"rust1"</span><span class="at">,</span> since <span class="at">=</span> <span class="st">"1.0.0"</span><span class="at">)]</span>
<span class="kw">pub</span> <span class="kw">enum</span> <span class="dt">Result</span>&lt;T, E&gt; <span class="op">{</span>
    <span class="co">/// Contains the success value</span>
    <span class="at">#[</span>stable<span class="at">(</span>feature <span class="at">=</span> <span class="st">"rust1"</span><span class="at">,</span> since <span class="at">=</span> <span class="st">"1.0.0"</span><span class="at">)]</span>
    <span class="cn">Ok</span>(<span class="at">#[</span>stable<span class="at">(</span>feature <span class="at">=</span> <span class="st">"rust1"</span><span class="at">,</span> since <span class="at">=</span> <span class="st">"1.0.0"</span><span class="at">)]</span> T),
    <span class="co">/// Contains the error value</span>

    <span class="at">#[</span>stable<span class="at">(</span>feature <span class="at">=</span> <span class="st">"rust1"</span><span class="at">,</span> since <span class="at">=</span> <span class="st">"1.0.0"</span><span class="at">)]</span>
    <span class="cn">Err</span>(<span class="at">#[</span>stable<span class="at">(</span>feature <span class="at">=</span> <span class="st">"rust1"</span><span class="at">,</span> since <span class="at">=</span> <span class="st">"1.0.0"</span><span class="at">)]</span> E),
<span class="op">}</span></code></pre>
</div>

Also here's a gist we want to change:

https://gist.github.com/71074de7a8f544ddd27ebec4b3f287dd

]]></content:encoded>
</item>
  XML
  # rubocop:enable Layout/IndentHeredoc

  EXPECTED_CODE_BLOCK = <<~PRE.freeze
    #[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
    #[must_use = "this `Result` may be an `Err` variant, which should be handled"]
    #[stable(feature = "rust1", since = "1.0.0")]
    pub enum Result<T, E> {
        /// Contains the success value
        #[stable(feature = "rust1", since = "1.0.0")]
        Ok(#[stable(feature = "rust1", since = "1.0.0")] T),

        /// Contains the error value
        #[stable(feature = "rust1", since = "1.0.0")]
        Err(#[stable(feature = "rust1", since = "1.0.0")] E),
    }
  PRE

  attr_reader :post

  def setup
    ReverseMarkdown.config.github_flavored = true
    node = Nokogiri::XML(EXAMPLE_DOC)
    @post = WpToHugo::Post.new(node.css('item').first)
  end

  def test_preserve_whitespace
    assert_includes(post.markdown, "linebreaks.\n\n")
  end

  def test_normal_code_works
    assert_includes(post.markdown, '`chmod` or `chown`')
  end

  def test_dont_mangle_code_whitespace
    assert_includes(post.markdown, "\n\n#")
  end

  def test_expand_gists
    assert_includes(post.markdown,
                    '<script src="https://gist.github.com/willmurphyscode/71074de7a8f544ddd27ebec4b3f287dd.js"></script>') # rubocop:disable Metrics/LineLength:
  end
end
