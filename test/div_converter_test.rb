require 'minitest/autorun'
require 'reverse_markdown'
require './lib/wp_to_hugo'

class DivConverterTest < Minitest::Test
  WP_NONSENSE_DIV = <<~HTML.freeze
      <div class="wp-nonsense">Some text

    With some

    paragraph breaks and a code sample that's not in its own div:

    <pre><code># not the working file yet
    FROM redis

    COPY start.sh /usr/bin/start.sh
    USER redis
    CMD ["/start.sh"]
    </code></pre>
    </div>
  HTML

  attr_reader :node, :converter

  def setup
    @converter = ReverseMarkdown::Converters::CustomDiv.new
    @node = Nokogiri::HTML.parse(WP_NONSENSE_DIV).root.child.child
  end

  def test_preserve_paragraphs
    assert_includes(converter.convert(node), "Some text\n\nWith some\n\nparagraph breaks")
  end

  def test_presever_raw_pre
    assert_includes(converter.convert(node), "FROM redis\n\n")
  end

  def test_presever_raw_pre_no_extra_backtick
    refute_includes(converter.convert(node), '`#')
  end
end
