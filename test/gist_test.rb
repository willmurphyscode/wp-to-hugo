require 'minitest/autorun'

require './lib/wp_to_hugo'

class GistTest < Minitest::Test
  attr_reader :post_fragment, :expected_script_tag
  def setup
    @post_fragment = <<~POST_FRAGMENT
      Here is a lonely gist that points to some nonsense I wrote:

      https://gist.github.com/willmurphyscode/59e5800026b60b8d18cca5508469b034

      We want to replace that with a script tag, and sometimes I want to just randomly have
      a [link to some gist](https://gist.github.com/willmurphyscode/f348c4006033484af6d410573c198d50)
      that I want preserved.
    POST_FRAGMENT

    @expected_script_tag = '<script src="https://gist.github.com/willmurphyscode/59e5800026b60b8d18cca5508469b034.js"></script>' # rubocop:disable Metrics/LineLength
  end

  def test_replace_lonely_gist_with_script_tag
    result = WpToHugo::Gist.all_to_script_tags @post_fragment
    assert_includes result, expected_script_tag
  end

  def test_ignore_other_gist
    result = WpToHugo::Gist.all_to_script_tags @post_fragment
    assert_includes result, '[link to some gist](https://gist.github.com/willmurphyscode/f348c4006033484af6d410573c198d50)'
  end

  def test_get_username_if_necessary
    has_anonymous_gist_link = <<~HTML
      Also here's a gist we want to change:

      https://gist.github.com/71074de7a8f544ddd27ebec4b3f287dd

      and some more content
    HTML

    result = WpToHugo::Gist.all_to_script_tags(has_anonymous_gist_link)
    assert_includes(result, '<script src="https://gist.github.com/willmurphyscode/71074de7a8f544ddd27ebec4b3f287dd.js"></script>') # rubocop:disable Metrics/LineLength
  end

  def test_follow_redirect
    url = 'https://gist.github.com/71074de7a8f544ddd27ebec4b3f287dd'
    expected_location = 'https://gist.github.com/willmurphyscode/71074de7a8f544ddd27ebec4b3f287dd'
    assert_equal(WpToHugo::Gist.follow_redirect(url), expected_location)
  end

  def test_always_includes_username
    url = 'https://gist.github.com/71074de7a8f544ddd27ebec4b3f287dd'
    expected_location = '<script src="https://gist.github.com/willmurphyscode/71074de7a8f544ddd27ebec4b3f287dd.js"></script>' # rubocop:disable Metrics/LineLength
    assert_equal(WpToHugo::Gist.embed_script_tag(url), expected_location)
  end

  def test_always_includes_username_mock_network
    url = 'https://gist.github.com/71074de7a8f544ddd27ebec4b3f287dd'
    WpToHugo::Gist.stub(:follow_redirect, 'https://gist.github.com/willmurphyscode/71074de7a8f544ddd27ebec4b3f287dd') do # rubocop:disable Metrics/LineLength
      expected_script_tag = '<script src="https://gist.github.com/willmurphyscode/71074de7a8f544ddd27ebec4b3f287dd.js"></script>' # rubocop:disable Metrics/LineLength
      assert_equal(WpToHugo::Gist.embed_script_tag(url), expected_script_tag)
    end
  end

  def test_include_username_true
    assert(WpToHugo::Gist.include_username?('https://gist.github.com/willmurphyscode/71074de7a8f544ddd27ebec4b3f287dd')) # rubocop:disable Metrics/LineLength
  end

  def test_include_username_false
    refute(WpToHugo::Gist.include_username?('https://gist.github.com/71074de7a8f544ddd27ebec4b3f287dd'))
  end
end
