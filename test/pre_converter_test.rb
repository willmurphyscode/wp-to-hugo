require 'minitest/autorun'
require 'reverse_markdown'
require './lib/wp_to_hugo'

class PreConverterTest < Minitest::Test
  EXAMPLE_NODE = <<~HTML.freeze
    <div class="sourceCode">
    <pre class="sourceCode r"><code class="sourceCode r">myList &lt;- <span class="kw">list</span>(<span class="dt">a =</span> <span class="dv">1</span>:<span class="dv">5</span>, <span class="dt">b =</span> <span class="dv">6</span>:<span class="dv">10</span>, <span class="dt">c=</span><span class="dv">11</span>:<span class="dv">15</span>)
    squares &lt;- <span class="kw">lapply</span>(myList, function(x) {x * x})
    squares
    $a
    [<span class="dv">1</span>]  <span class="dv">1</span>  <span class="dv">4</span>  <span class="dv">9</span> <span class="dv">16</span> <span class="dv">25</span>

    $b
    [<span class="dv">1</span>]  <span class="dv">36</span>  <span class="dv">49</span>  <span class="dv">64</span>  <span class="dv">81</span> <span class="dv">100</span>

    $c
    [<span class="dv">1</span>] <span class="dv">121</span> <span class="dv">144</span> <span class="dv">169</span> <span class="dv">196</span> <span class="dv">225</span></code></pre>
    </div>
  HTML

  attr_reader :node, :converter, :unmarked_pre

  def setup
    ReverseMarkdown.config.github_flavored = true
    @converter = ReverseMarkdown::Converters::CustomPre.new
    @node = Nokogiri::HTML.parse(EXAMPLE_NODE).root.child.child
    @unmarked_pre = <<~HTML
      <pre><code># not the working file yet
      FROM redis

      COPY start.sh /data/start.sh
      USER redis
      CMD ["/data/start.sh"]
      </code></pre>
    HTML
  end

  def test_preserves_wordpress_style_whitespace
    assert_includes(converter.convert(node), "\n\n$c")
  end

  def test_inserts_correct_language
    assert_includes(converter.convert(node), '```r')
  end

  def test_unmarked_pre_is_multiline
    code_within_pre = Nokogiri::HTML.parse(unmarked_pre).root.child.child
    assert_includes(converter.convert(code_within_pre), "FROM redis\n\n")
  end
end
