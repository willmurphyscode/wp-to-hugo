module WpToHugo
  class PostWriter
    attr_reader :dest
    def initialize(dest_folder)
      @dest = dest_folder
    end

    def write_to_hugo(post)
      path = File.join(dest, post.filename)
      File.write(path, post.file_body)
    end
  end
end
