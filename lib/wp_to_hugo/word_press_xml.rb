module WpToHugo
  class WordPressXML
    attr_reader :doc

    def initialize(nokogiri_doc)
      @doc = nokogiri_doc
    end

    def posts
      doc.css('item')
    end
  end
end
