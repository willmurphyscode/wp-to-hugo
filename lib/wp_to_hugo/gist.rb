require 'net/http'
require 'uri'

module WpToHugo
  class Gist
    GIST_URL_REGEX = Regexp.new('\s+(https:\/\/gist.github.com(\/\w+)?\/[0-9a-f]{32})\s+').freeze
    # example: https://gist.github.com/willmurphyscode/8e1f38e1f693d999d13c3592d9c25efa
    def self.embed_script_tag(pretty_url)
      gist_url = pretty_url
      gist_url = follow_redirect(pretty_url) unless include_username?(gist_url)
      "<script src=\"#{gist_url}.js\"></script>"
    end

    def self.follow_redirect(url)
      url = URI.parse(url)
      response = Net::HTTP.get_response(url)
      response['location']
    end

    def self.include_username?(gist_url)
      %r{https://gist.github.com/\w+/[0-9a-f]{32}}.match? gist_url
    end

    def self.all_to_script_tags(markdown_string)
      # TODO: something like https://github.com/shinnn/github-username-regex
      to_return = markdown_string
      matches = markdown_string.scan(GIST_URL_REGEX)
      matches.each do |match|
        to_return.sub!(match.first, embed_script_tag(match.first))
      end
      to_return
    end
  end
end
