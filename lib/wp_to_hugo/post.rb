module WpToHugo
  class Post
    attr_reader :node
    def initialize(item_node)
      @node = item_node
    end

    def title
      node.css('title').first.text
    end

    def post_name
      node.xpath('./wp:post_name').first.text
    end

    def date
      # 2018-05-30 13:09:14 ==> 2018-05-30
      node.xpath('./wp:post_date').first.text.split[0]
    end

    def filename
      "#{date}-#{post_name}.md"
    end

    def raw_html
      node.xpath('./content:encoded').text
    end

    def tags
      node.css('category')
          .map { |category_node| category_node['nicename'] }
          .reject { |tag_name| tag_name == 'uncategorized' }
    end

    def tag_string
      tags.to_s
    end

    def markdown
      html_string = "<div class=\"wp-nonsense\">#{raw_html}</div>"
      md = ReverseMarkdown.convert html_string
      Gist.all_to_script_tags md
    end

    def metadata_block
      <<~META
        ---
        title: "#{title}"
        date: #{date}
        tags: #{tag_string}
        ---
      META
    end

    def file_body
      "#{metadata_block}\n\n#{markdown}"
    end

    def draft?
      node.xpath('./wp:status').text != 'publish'
    end

    def post?
      node.xpath('./wp:post_type').text == 'post'
    end

    def skip?
      draft? || !post?
    end
  end
end
