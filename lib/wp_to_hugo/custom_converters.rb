module ReverseMarkdown
  module Converters
    class CustomPre < Base
      def convert(node, state = {})
        content = treat_children(node, state.merge(within_pre: true))
        if ReverseMarkdown.config.github_flavored
          "\n```#{language(node)}\n" << content.strip << "\n```\n"
        else
          "\n\n    " << content.lines.to_a.join('    ') << "\n\n"
        end
      end

      private

      def language(node)
        # TODO: language from best guess
        lang = language_from_wp_class(node)
        lang ||= language_from_highlight_class(node)
        lang || language_from_confluence_class(node)
      end

      def language_from_wp_class(node)
        classes = node['class']&.split || []
        index = classes.find_index 'sourceCode'
        return if index.nil?

        classes[index + 1]
      end

      def language_from_highlight_class(node)
        node.parent['class'].to_s[/highlight-([a-zA-Z0-9]+)/, 1]
      end

      def language_from_confluence_class(node)
        node['class'].to_s[/brush:\s?(:?.*);/, 1]
      end
    end

    register :pre, CustomPre.new

    class CustomDiv < Div
      def convert(node, state = {})
        super unless node['class'] == 'wp-nonsense'
        output = ''
        node.children.each do |child|
          if child.name == 'text'
            output << child.text
            next
          end
          output << treat(child, state)
        end
        output
      end
    end

    register :div, CustomDiv.new

    class CustomCode < Code
      def convert(node, state = {})
        return node.text if state[:within_pre]

        super
      end
    end
    register :code, CustomCode.new
  end
end
